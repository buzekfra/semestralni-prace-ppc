from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
import time
import math
import copy

class Kalkulačka_Matic(Tk):
    def __init__(self):
        super(Kalkulačka_Matic, self).__init__()
        self.title("Kalkulačka Matic")
        self.minsize(780, 400) 

        OvládáníKaret = ttk.Notebook(self)
        self.Karta1 = ttk.Frame(OvládáníKaret)
        OvládáníKaret.add(self.Karta1, text = "  Výpočet determinantu matice  ")

        self.Karta2 = ttk.Frame(OvládáníKaret)
        OvládáníKaret.add(self.Karta2, text = "  Operace se dvěma maticemi  ")
            
        OvládáníKaret.pack(expan = 1, fill = 'both')

        self.karta1()
        self.karta2()

    def karta1(self):
        L1 = ttk.Label(self.Karta1, text = " ")
        L2 = ttk.Label(self.Karta1, text = "Výpočet determinantu libovolně velké čtvercové matice načtené ze souboru pomocí kondenzační metody.")
        L1.pack()
        L2.pack(pady = 10)
        
        LF1 = ttk.LabelFrame(self.Karta1, text = "  Čtvercová matice musí být uložena a zapsanána podle těchto pravidel:  ")
        ttk.Style().configure(self.Karta1, background = '#00979C')
        LF1.pack(pady = 10)

        L3 = ttk.Label(LF1, text = "            1) Matice musí být uložena v textovém souboru (.txt) s kódováním UTF-8.")
        L4 = ttk.Label(LF1, text = "            1) Jednotlivé členy matice musí být z množiny CELÝCH ČÍSEL (... -2, -1, 0, 1, 2, ...).      ")
        L5 = ttk.Label(LF1, text = "            2) Jednotlivé členy matice jsou v řádce oddělovány mezerou.")
        L6 = ttk.Label(LF1, text = "            3) Za posledním členem každé řádky musí být čárka.")
        L3.grid(column = 0, row = 0, sticky = 'W')
        L4.grid(column = 0, row = 1, sticky = 'W')
        L5.grid(column = 0, row = 2, sticky = 'W')
        L6.grid(column = 0, row = 3, sticky = 'W')

        L7 = ttk.Label(LF1, text = "            ")
        L8 = ttk.Label(LF1, text = " ")
        L7.grid(column = 2, row = 0, sticky = 'W')
        L8.grid(column = 1, row = 4)   

        LF2 = ttk.LabelFrame(LF1, text = "  Příklad zápisu:  ")
        LF2.grid(column = 1, row = 0, rowspan = 4)

        L9 = ttk.Label(LF2, text = "    1     3    12    -5,   ")
        L10 = ttk.Label(LF2, text = "   -1     0    76     0,   ")
        L11 = ttk.Label(LF2, text = "   20     8     9     3,   ")
        L12 = ttk.Label(LF2, text = "    4    -6     2    64,   ")
        L9.grid(column = 0, row = 0, sticky = 'W')
        L10.grid(column = 0, row = 1, sticky = 'W')
        L11.grid(column = 0, row = 2, sticky = 'W')
        L12.grid(column = 0, row = 3, sticky = 'W')

        LF3 = ttk.LabelFrame(self.Karta1, text = "  Nahraj soubor:  ")
        LF3.pack(pady = 10, padx = 10)
        
        L13 = ttk.Label(LF3, text = "            Vybraný soubor:   ")
        L14 = ttk.Label(LF3, text = "Není vybrán žádný soubor")
        L15 = ttk.Label(LF3, text = "   ")
        L16 = ttk.Label(LF3, text = "            ")
        L17 = ttk.Label(LF3, text = " ")
        L13.grid(column = 0, row = 0, sticky = 'W', pady = 15)
        L14.grid(column = 1, row = 0, sticky = 'W')
        L15.grid(column = 2, row = 0, sticky = 'W')
        L16.grid(column = 6, row = 0, sticky = 'W')
        L17.grid(column = 4, row = 0)

        L14.configure(foreground = '#ff00ff')

        L18 = ttk.Label(self.Karta1, text = "Není vypočítán žádný determinant.")
        L18.pack(pady = 10)

        L18.configure(foreground = '#ff00ff')
        self.Karta1.Vybrano = int(0)

        
        def Vyber_soubor():
            nazev_souboru = filedialog.askopenfilename(initialdir = '/', title = "Vyber soubor", filetype = (("Text File", "*.txt"), ("All Files", "*.*")))
            if nazev_souboru != '':
                L14.configure(text = nazev_souboru)
                L14.configure(foreground = '#0066ff')
                self.Karta1.Vybrano = int(1)
                self.Karta1.vybrany_soubor_1 = copy.deepcopy(nazev_souboru)

        B1 = ttk.Button(LF3, text = "  Vybrat  ", command = Vyber_soubor)
        B1.grid(column = 3, row = 0, sticky = 'W')

        LF4 = ttk.LabelFrame(self.Karta1, text = "  Řešení kondenzační metodou:  ")
        LF4.pack(pady = 10, padx = 10)
        
        LB1 = Listbox(LF4, height = 7, width = 80, justify = CENTER)
        Scroll1 = Scrollbar(LF4, command = LB1.yview)

        LB1.configure(yscrollcommand = Scroll1.set)
        LB1.pack(side = LEFT)

        Scroll1.pack(side = RIGHT, fill = Y)


        def Reseni_matice():
            with open(self.Karta1.vybrany_soubor_1, encoding='utf-8') as soubor:
                Matice = soubor.read()
            lengh = int(len(Matice))
            pocet_clenu_radku = list()
            mu = list()
            z = int(1)
            for i in range(1,lengh):
                x = int(0)
                j = int(i-1)
                if((Matice[i] == " " and ((Matice[j]=="0")or(Matice[j]=="1")or(Matice[j]=="2")or(Matice[j]=="3")or(Matice[j]=="4")or(Matice[j]=="5")or(Matice[j]=="6")or(Matice[j]=="7")or(Matice[j]=="8")or(Matice[j]=="9"))) or Matice[i] == ","):
                    if(z == 1):
                        line = list()
                        z = 0
                    while((Matice[j] != " ")  and  (Matice[j] != ",") and (j>=0)):
                        if((Matice[j]=="0")or(Matice[j]=="1")or(Matice[j]=="2")or(Matice[j]=="3")or(Matice[j]=="4")or(Matice[j]=="5")or(Matice[j]=="6")or(Matice[j]=="7")or(Matice[j]=="8")or(Matice[j]=="9")):
                            x = x + ((int(Matice[j]) * (10**((i-j)-1)) ))
                        elif(Matice[j] == "-"):
                            x = int(x*(-1))
                        j -= 1
                    line.append(int(x))
                    if(Matice[i] == ","):
                        pocet_clenu_radku.append(len(line))
                        mu.append(line)	
                        line = list()
            a = int(len(mu))
            ctvercova_matice = int(1)
            print("________________________________________________________________________________")
            LB1.insert(END,("________________________________________________________________________________"))
            for i in range(0,len(pocet_clenu_radku)):
                if (pocet_clenu_radku[i] != len(pocet_clenu_radku)):
                    ctvercova_matice = 0
            if(ctvercova_matice == 1):
                print("Ze souboru byla načtena čtvercová matice o velikosti %sx%s : "%(a,pocet_clenu_radku[0]))
                print(Matice)
                print("Matice byla uložena do 2D pole :")
                LB1.insert(END,("Ze souboru byla načtena čtvercová matice o velikosti %sx%s : "%(a,pocet_clenu_radku[0])))
                LB1.insert(END,("Matice byla uložena do 2D pole :"))
                for i in range(0,a):
                    print(mu[i])
                    LB1.insert(END,("%s"%(mu[i])))
                    LB1.see(END)
                vysledek = int()
                p = int(1)
                if(a>2):
                    m = list()
                    m = copy.deepcopy(mu)
                    n = int(a)
                    p = int(1)
                    while (n>2):
                        p = p*(1/((mu[0][0])**(n-2)))
                        for sloupec in range(0, n-1):
                            for radka in range(0, n-1):
                                m[radka][sloupec]= (((mu[0][0])*(mu[1+radka][1+sloupec])) - ((mu[1+radka][0])*(mu[0][1+sloupec])))
                        for radek in range(0, n):
                            del(m[radek][n-1])
                        del(m[n-1])
                        n = n-1
                        mu = copy.deepcopy(m)
                        print("Matice byla pomocí kondenzační metody upravena :")
                        print("D = %s ×"%(p))
                        LB1.insert(END,("Matice byla pomocí kondenzační metody upravena :"))
                        LB1.insert(END,("D = %s ×"%(p)))
                        for i in range(0,n):
                            print(mu[i])
                            LB1.insert(END,("%s"%(mu[i])))
                    a = n 
                if(a == 2):
                    vysledek = (p)* (((mu[0][0])*(mu[1][1]))-((mu[0][1])*(mu[1][0])))
                    print("Determinant matice řešené kondenzační metodou je: ", round(vysledek))
                    LB1.insert(END,("Determinant matice řešené kondenzační metodou je :"))
                    LB1.insert(END,("D = %s"%(round(vysledek))))
                    L18.configure(text = ("Determinant vybrané matice je %s"%(round(vysledek))))
                    L18.configure(foreground = '#0066ff')
            elif(ctvercova_matice != 1):
                print("Ve vybraném souboru není správně zapsaná čtvercová matice!")
                LB1.insert(END,("Ve vybraném souboru není správně zapsaná čtvercová matice!"))
                LB1.see(END)
                L18.configure(text = "Ve vybraném souboru není správně zapsaná čtvercová matice!")
                L18.configure(foreground = '#ff00ff')
                messagebox.showerror("Matice není čtvercová.", "Ve vybraném souboru není čtvercová matice. Vyberte soubor se správně zapsanou čtvercovou maticí.")

        def Vypocitat():
            if(self.Karta1.Vybrano == 0):
                messagebox.showerror("Vyber soubor.", "Není vybrán soubor s maticí. Nejdříve vyberte soubor.")
            elif(self.Karta1.Vybrano == 1):
                Reseni_matice()
                

        B2 = ttk.Button(LF3, text = "  Vypočítat  ", command = Vypocitat)
        B2.grid(column = 5, row = 0, sticky = 'W')




    def karta2(self):
        L1 = ttk.Label(self.Karta2, text = " ")
        L2 = ttk.Label(self.Karta2, text = "Matematické operace se dvěma maticemi.")
        L1.pack()
        L2.pack(pady = 10)
        
        LF1 = ttk.LabelFrame(self.Karta2, text = "  Matice musí být uloženy a zapsanány podle těchto pravidel:  ")
        ttk.Style().configure(self.Karta2, background = '#00979C')
        LF1.pack(pady = 10)

        L3 = ttk.Label(LF1, text = "            1) Jednotlivé matice musí být uloženy v textových souborech (.txt) s kódováním UTF-8.      ")
        L4 = ttk.Label(LF1, text = "            1) Jednotlivé členy matic musí být z množiny CELÝCH ČÍSEL (... -2, -1, 0, 1, 2, ...).")
        L5 = ttk.Label(LF1, text = "            2) Jednotlivé členy matic jsou v řádkách oddělovány mezerou.")
        L6 = ttk.Label(LF1, text = "            3) Za posledním členem každé řádky matice musí být čárka.")
        L3.grid(column = 0, row = 0, sticky = 'W')
        L4.grid(column = 0, row = 1, sticky = 'W')
        L5.grid(column = 0, row = 2, sticky = 'W')
        L6.grid(column = 0, row = 3, sticky = 'W')

        L7 = ttk.Label(LF1, text = "            ")
        L8 = ttk.Label(LF1, text = " ")
        L7.grid(column = 2, row = 0, sticky = 'W')
        L8.grid(column = 1, row = 4)   

        LF2 = ttk.LabelFrame(LF1, text = "  Příklad zápisu:  ")
        LF2.grid(column = 1, row = 0, rowspan = 4)

        L9 = ttk.Label(LF2, text = "    1     3    12    -5,   ")
        L10 = ttk.Label(LF2, text = "   -1     0    76     0,   ")
        L11 = ttk.Label(LF2, text = "   20     8     9     3,   ")
        L12 = ttk.Label(LF2, text = "    4    -6     2    64,   ")
        L9.grid(column = 0, row = 0, sticky = 'W')
        L10.grid(column = 0, row = 1, sticky = 'W')
        L11.grid(column = 0, row = 2, sticky = 'W')
        L12.grid(column = 0, row = 3, sticky = 'W')

        LF3 = ttk.LabelFrame(self.Karta2, text = "  Nahraj soubor:  ")
        LF3.pack(pady = 10, padx = 10)

        LF4 = ttk.LabelFrame(LF3, text = "  Matice 1:  ")
        LF5 = ttk.LabelFrame(LF3, text = "  Matice 2:  ")
        LF4.pack(side = LEFT)
        LF5.pack(side = RIGHT)
        
        L13 = ttk.Label(LF4, text = "Vybraný soubor matice 1:")
        L14 = ttk.Label(LF4, text = "Není vybrán žádný soubor")
        L13.pack(pady = 5, padx = 10)
        L14.pack(pady = 5, padx = 10)
        
        L14.configure(foreground = '#ff00ff')

        self.Karta2.Vybrano_1 = int(0)
        
        def Vyber_soubor_1():
            nazev_souboru = filedialog.askopenfilename(initialdir = '/', title = "Vyber soubor", filetype = (("Text File", "*.txt"), ("All Files", "*.*")))
            if nazev_souboru != '':
                L14.configure(text = nazev_souboru)
                L14.configure(foreground = '#0066ff')
                self.Karta2.Vybrano_1 = int(1)
                self.Karta2.vybrany_soubor_1 = copy.deepcopy(nazev_souboru)

        B1 = ttk.Button(LF4, text = "  Vybrat  ", command = Vyber_soubor_1)
        B1.pack(pady = 5, padx = 10)

        L15 = ttk.Label(LF5, text = "Vybraný soubor matice 2:")
        L16 = ttk.Label(LF5, text = "Není vybrán žádný soubor")
        L15.pack(pady = 5, padx = 10)
        L16.pack(pady = 5, padx = 10)
        
        L16.configure(foreground = '#ff00ff')

        self.Karta2.Vybrano_2 = int(0)

        def Vyber_soubor_2():
            nazev_souboru = filedialog.askopenfilename(initialdir = '/', title = "Vyber soubor", filetype = (("Text File", "*.txt"), ("All Files", "*.*")))
            if nazev_souboru != '':
                L16.configure(text = nazev_souboru)
                L16.configure(foreground = '#0066ff')
                self.Karta2.Vybrano_2 = int(1)
                self.Karta2.vybrany_soubor_2 = copy.deepcopy(nazev_souboru)

        B2 = ttk.Button(LF5, text = "  Vybrat  ", command = Vyber_soubor_2)
        B2.pack(pady = 5, padx = 10)
        

        LF6 = ttk.LabelFrame(self.Karta2, text = "  Vyber matematickou operaci:  ")
        LF6.pack(pady = 10, padx = 10)
        
        Operace = StringVar()
        
        L17 = ttk.Label(LF6, text = "            Vybraná matematická operace :   ")
        L18 = ttk.Label(LF6, text = "   ")
        L19 = ttk.Label(LF6, text = "            ")
        L17.grid(column = 0, row = 0, sticky = 'W', pady = 15)
        L18.grid(column = 2, row = 0, sticky = 'W', pady = 15)
        L19.grid(column = 4, row = 0, sticky = 'W', pady = 15)

        C1 = ttk.Combobox(LF6, width = 15, textvariable = Operace)
        C1['values'] = ("Sčítání", "Odčítání")
        C1.grid(column = 1, row = 0)


        LF7 = ttk.LabelFrame(self.Karta2, text = "  Řešení :  ")
        LF7.pack(pady = 10, padx = 10)
        
        LB1 = Listbox(LF7, height = 7, width = 80, justify = CENTER)
        Scroll1 = Scrollbar(LF7, command = LB1.yview)

        LB1.configure(yscrollcommand = Scroll1.set)
        LB1.pack(side = LEFT)

        Scroll1.pack(side = RIGHT, fill = Y)


        def Scitani_matic():
            dve_matice = list()
            pocet_radku_matice = int(0)
            pocet_sloupcu_matice = int(0)
            for dvakrat in range(0,2):
                if(dvakrat == 0):
                    with open(self.Karta2.vybrany_soubor_1, encoding='utf-8') as soubor:
                        Matice = soubor.read()
                elif(dvakrat == 1):
                    with open(self.Karta2.vybrany_soubor_2, encoding='utf-8') as soubor:
                        Matice = soubor.read()
                lengh = int(len(Matice))
                mu = list()
                z = int(1)
                for i in range(1,lengh):
                    x = int(0)
                    j = int(i-1)
                    if((Matice[i] == " " and ((Matice[j]=="0")or(Matice[j]=="1")or(Matice[j]=="2")or(Matice[j]=="3")or(Matice[j]=="4")or(Matice[j]=="5")or(Matice[j]=="6")or(Matice[j]=="7")or(Matice[j]=="8")or(Matice[j]=="9"))) or Matice[i] == ","):
                        if(z == 1):
                            line = list()
                            z = 0
                        while((Matice[j] != " ")  and  (Matice[j] != ",") and (j>=0)):
                            if((Matice[j]=="0")or(Matice[j]=="1")or(Matice[j]=="2")or(Matice[j]=="3")or(Matice[j]=="4")or(Matice[j]=="5")or(Matice[j]=="6")or(Matice[j]=="7")or(Matice[j]=="8")or(Matice[j]=="9")):
                                x = x + ((int(Matice[j]) * (10**((i-j)-1)) ))
                            elif(Matice[j] == "-"):
                                x = int(x*(-1))
                            j -= 1
                        line.append(int(x))
                        if(Matice[i] == ","):
                            mu.append(line)	
                            line = list()
                dve_matice.append(mu)
                if(len(mu) > pocet_radku_matice):
                    pocet_radku_matice = len(mu)
                if(len(mu[0]) > pocet_sloupcu_matice):
                    pocet_sloupcu_matice = len(mu[0])


            mS = list()
            for i in range(0, pocet_radku_matice):
                lS = list()
                for j in range(0, pocet_sloupcu_matice):
                    lS.append(0)
                mS.append(lS)
            print("________________________________________________________________________________")
            print("Z vybraných souborů byly načteny matice o těchto velikostech:")
            print("První matice má velikost %sx%s"%(len(dve_matice[0]),len(dve_matice[0][0])))
            print("Druhá matice má velikost %sx%s"%(len(dve_matice[1]),len(dve_matice[1][0])))
            print("Matice z vybraných souborů byly uložey do 3D pole.")
            print("Byla zvolena operace %s."%(Operace.get()))
            LB1.insert(END,("________________________________________________________________________________"))
            LB1.insert(END,("Z vybraných souborů byly načteny matice o těchto velikostech:"))
            LB1.insert(END,("První matice má velikost %sx%s"%(len(dve_matice[0]),len(dve_matice[0][0]))))
            LB1.insert(END,("Druhá matice má velikost %sx%s"%(len(dve_matice[1]),len(dve_matice[1][0]))))
            LB1.insert(END,("Matice z vybraných souborů byly uložey do 3D pole."))
            LB1.insert(END,("Byla zvolena operace %s."%(Operace.get())))
            for i in range(0,len(dve_matice)):
                for j in range(0,len(dve_matice[i])):
                    print(dve_matice[i][j])
                    LB1.insert(END,("%s"%(dve_matice[i][j])))
                if(i == (len(dve_matice) - 1)):
                    print(" = ")
                    LB1.insert(END,(" = "))
                else:
                    if(Operace.get() == "Sčítání"):
                        print(" + ")
                        LB1.insert(END,(" + "))
                    elif(Operace.get() == "Odčítání"):
                        print(" - ")
                        LB1.insert(END,(" - "))

            for s in range(0,len(dve_matice)):
                for i in range(0,len(dve_matice[s])):
                    for j in range(0,len(dve_matice[s][i])):
                        h = int(mS[i][j])
                        if(Operace.get() == "Sčítání"):
                            mS[i][j] = (h + dve_matice[s][i][j])
                        elif(Operace.get() == "Odčítání"):
                            if(s == 0):
                                mS[i][j] = (h + dve_matice[s][i][j])
                            elif(s > 0):
                                mS[i][j] = (h - dve_matice[s][i][j])

            for i in range(0,len(mS)):
                print(mS[i])
                LB1.insert(END,("%s"%(mS[i])))
                LB1.see(END)            


        def Vypocitat():
            if(self.Karta2.Vybrano_1 == 1 and self.Karta2.Vybrano_2 == 1 and (Operace.get() == "Sčítání" or Operace.get() == "Odčítání")):
                Scitani_matic()

            elif(self.Karta2.Vybrano_1 == 0 and self.Karta2.Vybrano_2 == 0 and Operace.get() != "Sčítání" and Operace.get() != "Odčítání" and Operace.get() != ""):
                messagebox.showerror("Vyber soubor a operaci.", "Nejsou vybrány žádné soubory s maticemi a není vybrána platná operace. Nejdříve vyberte soubory s maticemi a zvolte platnou operaci s maticemi.")
            elif(self.Karta2.Vybrano_1 == 0 and self.Karta2.Vybrano_2 == 0 and Operace.get() == ""):
                messagebox.showerror("Vyber soubor a operaci.", "Nejsou vybrány žádné soubory s maticemi a není vybrána operace. Nejdříve vyberte soubory s maticemi a zvolte operaci s maticemi.")

            elif(self.Karta2.Vybrano_1 == 0 and self.Karta2.Vybrano_2 == 1 and Operace.get() != "Sčítání" and Operace.get() != "Odčítání" and Operace.get() != ""):
                messagebox.showerror("Vyber soubor a operaci.", "Není vybrán soubor s první maticí a není vybrána platná operace. Nejdříve vyberte soubor s první maticí a zvolte platnou operaci s maticemi.")
            elif(self.Karta2.Vybrano_1 == 0 and self.Karta2.Vybrano_2 == 1 and Operace.get() == ""):
                messagebox.showerror("Vyber soubor a operaci.", "Není vybrán soubor s první maticí a není vybrána operace. Nejdříve vyberte soubor s první maticí a zvolte operaci s maticemi.")

            elif(self.Karta2.Vybrano_1 == 1 and self.Karta2.Vybrano_2 == 0 and Operace.get() != "Sčítání" and Operace.get() != "Odčítání" and Operace.get() != ""):
                messagebox.showerror("Vyber soubor a operaci.", "Není vybrán soubor s druhou maticí a není vybrána platná operace. Nejdříve vyberte soubor s druhou maticí a zvolte platnou operaci s maticemi.")
            elif(self.Karta2.Vybrano_1 == 1 and self.Karta2.Vybrano_2 == 0 and Operace.get() == ""):
                messagebox.showerror("Vyber soubor a operaci.", "Není vybrán soubor s druhou maticí a není vybrána operace. Nejdříve vyberte soubor s druhou maticí a zvolte operaci s maticemi.")

            elif(self.Karta2.Vybrano_1 == 0 and self.Karta2.Vybrano_2 == 0 and (Operace.get() == "Sčítání" or Operace.get() == "Odčítání")):
                messagebox.showerror("Vyber soubor.", "Nejsou vybrány žádné soubory s maticemi. Nejdříve vyberte soubory s maticemi.")
            elif(self.Karta2.Vybrano_1 == 0 and self.Karta2.Vybrano_2 == 1 and (Operace.get() == "Sčítání" or Operace.get() == "Odčítání")):
                messagebox.showerror("Vyber soubor.", "Není vybrán soubor s první maticí. Nejdříve vyberte soubor s první maticí.")
            elif(self.Karta2.Vybrano_1 == 1 and self.Karta2.Vybrano_2 == 0 and (Operace.get() == "Sčítání" or Operace.get() == "Odčítání")):
                messagebox.showerror("Vyber soubor.", "Není vybrán soubor s druhou maticí. Nejdříve vyberte soubor s druhou maticí.")

            elif(self.Karta2.Vybrano_1 == 1 and self.Karta2.Vybrano_2 == 1 and Operace.get() != "Sčítání" and Operace.get() != "Odčítání" and Operace.get() != ""):
                messagebox.showerror("Vyber operaci.", "Není vybrána platná operace. Nejdříve zvolte platnou operaci s maticemi.")
            elif(self.Karta2.Vybrano_1 == 1 and self.Karta2.Vybrano_2 == 1 and Operace.get() == ""):
                messagebox.showerror("Vyber operaci.", "Není vybrána operace. Nejdříve zvolte operaci s maticemi.")

        B3 = ttk.Button(LF6, text = "  Vypočítat  ", command = Vypocitat)
        B3.grid(column = 3, row = 0, sticky = 'W', pady = 15)

root = Kalkulačka_Matic()
root.mainloop()



